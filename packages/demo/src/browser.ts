import super80 from "super-eighty-client";

const app = super80();

app.use(async function*(req, res) {
  yield res.status(200);
  yield res.header("Content-Type", "text/html");
  yield res.send(
    `Hey! This is here is served from your browser. Follow <a href="https://demo.proxy.http.pbedat.de">this link</a> to the proxy in <code>./demo.ts</code>`
  );
});

app.listen("browser-demo");

window.document.body.innerHTML = `<strong>super80</strong> listening to: <a href="https://browser-demo.proxy.http.pbedat.de">browser-demo.http.proxy.pbedat.de</a>`;
