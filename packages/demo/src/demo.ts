import super80, { proxy } from "super-eighty-client";
import express from "express";
import { readFileSync } from "fs";

const projected = super80();

projected.use(async function*(req, res) {
  yield res.status(200);
  console.log(req.path);
  switch (req.path) {
    case "/":
      yield res.header("content-type", "text/html");
      yield res.send(
        'This is the <code>demo.ts</code>. Check this path: <a href="./demo">DEMO</a>'
      );
      break;
    case "/demo":
      yield res.send("this is a demo");

    case "/picture.jpeg":
      yield res.header("content-type", "image/jpeg");
      yield res.send(
        readFileSync(__dirname + "/200.jpeg").toString("base64"),
        "base64"
      );
      break;
  }
});

projected.listen("demo");

const app = express();

app.get("/", (req, res) => res.send("I was forwarded"));

app.listen(8899, () => console.log("demo listening to http://127.0.0.1:8899"));

const proxied = super80();

proxied.use(proxy("http://127.0.0.1:8899"));
proxied.listen("forwarded");
