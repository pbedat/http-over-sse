import UniversalEventSource from "eventsource";
import { IncomingHttpHeaders, OutgoingHttpHeaders } from "http";
import universalFetch, { Headers } from "node-fetch";

var fetch =
  typeof universalFetch !== "function" ? window.fetch : universalFetch;

var EventSource =
  typeof UniversalEventSource !== "function"
    ? ((window as any)["EventSource"] as any)
    : UniversalEventSource;

interface Options {
  logger: Logger;
  baseUrl: string;
}
interface Logger {
  debug(msg: string): void;
  error(msg: string): void;
}

const defaultOptions = {
  logger: console,
  baseUrl: "http://http.pbedat.de"
};

export function proxy(baseUrl: string): Middleware {
  return async function*(req, res) {
    const response = await fetch(`${baseUrl}${req.path}`, {
      method: req.method,
      headers: req.headers as Record<string, string>,
      ...(!["GET", "HEAD"].includes(req.method) ? { body: req.body } : {})
    });

    for (const [name, value] of response.headers) {
      yield res.header(name, value);
    }

    yield res.status(response.status);

    const body = await response.text();

    yield res.send(body);
  };
}

export default function projector(options: Partial<Options> = defaultOptions) {
  const initializedOptions = { ...defaultOptions, ...options } as Options;
  const { logger, baseUrl } = initializedOptions;

  let middlewares: Middleware[] = [];

  function use(middleware: Middleware) {
    middlewares = [...middlewares, middleware];
  }

  function listen(name: string) {
    const eventSourceUrl = `${baseUrl}/api/${name}.proxy.http.pbedat.de/requests`;

    const events = new EventSource(eventSourceUrl);

    events.addEventListener("open", () => {
      logger.debug(`connection established: ${eventSourceUrl}`);
    });

    events.addEventListener("error", () => {
      logger.error(`connection failed: ${eventSourceUrl}`);
    });

    const handler = createServer(name, middlewares, initializedOptions);

    events.addEventListener("message", handler);

    return () => {
      events.close();
    };
  }

  return {
    use,
    listen
  };
}

function createServer(
  name: string,
  middlewares: Middleware[],
  options: Options
) {
  const { logger, baseUrl } = options;

  async function handler(ev: MessageEvent) {
    const { uid, request } = JSON.parse(ev.data) as ProxyRequestMessage;
    logger.debug(`received request "${uid}"`);
    logger.debug(JSON.stringify(request, null, 2));

    let proxyResponse: ProxyResponse = {
      status: 404,
      body: "",
      encoding: "utf8",
      headers: {}
    };

    for (const middleware of middlewares) {
      for await (const effect of middleware(request, createResponseApi())) {
        switch (effect.type) {
          case "SET_HEADER": {
            proxyResponse = {
              ...proxyResponse,
              headers: {
                ...proxyResponse.headers,
                [effect.name]: effect.value
              }
            };
            break;
          }
          case "SET_STATUS": {
            proxyResponse = { ...proxyResponse, status: effect.status };
            break;
          }
          case "WRITE_BODY": {
            proxyResponse = {
              ...proxyResponse,
              encoding: effect.encoding || "utf8",
              body: proxyResponse.body + effect.chunk
            };
            break;
          }
        }
      }

      const response = await fetch(
        `${baseUrl}/api/${name}.proxy.http.pbedat.de/requests/${uid}/responses`,
        {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(proxyResponse)
        }
      );

      if (!response.ok) {
        logger.error(`response transmission failed: ${response.status}`);
      }
    }
  }

  return handler as (ev: MessageEvent | Event) => void;
}

interface ProxyResponse {
  status: number;
  headers: IncomingHttpHeaders;
  body: string;
  encoding: "utf8" | "base64";
}

type Middleware = (
  req: ProxyRequest,
  res: ReturnType<typeof createResponseApi>
) => ResponseIterator;

type ResponseIterator = AsyncIterableIterator<HttpResponseEffects>;

type HttpResponseEffects = HeaderEffect | StatusEffect | WriteBodyEffect;

interface HeaderEffect {
  readonly type: "SET_HEADER";
  readonly name: keyof OutgoingHttpHeaders;
  readonly value: string;
}

interface StatusEffect {
  readonly type: "SET_STATUS";
  readonly status: number;
}

interface WriteBodyEffect {
  readonly type: "WRITE_BODY";
  readonly encoding: "utf8" | "base64";
  readonly chunk: string;
}

function createResponseApi() {
  return {
    header(name: keyof OutgoingHttpHeaders, value: string): HeaderEffect {
      return { type: "SET_HEADER", name, value };
    },
    status(status: number): StatusEffect {
      return { type: "SET_STATUS", status };
    },
    send(chunk: string, encoding?: "utf8" | "base64"): WriteBodyEffect {
      return { type: "WRITE_BODY", chunk, encoding: encoding || "utf8" };
    }
  };
}

interface ProxyRequestMessage {
  uid: string;
  request: ProxyRequest;
}

interface ProxyRequest {
  method: string;
  query: Record<string, string>;
  path: string;
  body: string;
  headers: IncomingHttpHeaders;
}
