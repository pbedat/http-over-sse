# http-over-sse

A http proxy routing requests to server-sent event streams and accepting responses through a rest api.

## Demo

```
npm start -- --port 8081
start http://127.0.0.1:8081/test.html
start http://localhost:8081/
```
