import { v4 as uuid } from "node-uuid";
import { without, omit } from "lodash";
import { Request } from "express";

type RequestMap = {
  requests: Record<
    string,
    {
      request: Express.Request;
      resolve: (response: any) => void;
      reject: (err: any) => void;
    }
  >;
  listeners: Listener[];
};

let domains: Record<string, RequestMap> = {};

export function pushRequest(domain: string, path: string, request: Request) {
  console.log("pushRequest", domain, Object.keys(domains));
  if (!domains[domain]) {
    return false;
  }

  return new Promise<any>((resolve, reject) => {
    const requestId = uuid();

    console.log("registering request %s", requestId);

    domains = {
      ...domains,
      [domain]: {
        ...domains[domain],
        requests: {
          ...domains[domain].requests,
          [requestId]: { request, resolve, reject }
        }
      }
    };

    const requestArgs = {
      method: request.method,
      path: path,
      body: request.body,
      query: request.query,
      headers: request.headers
    };

    domains[domain].listeners.forEach(listener =>
      listener(requestId, requestArgs)
    );

    request.on("error", () => removeRequest(domain, requestId));
    request.on("close", () => removeRequest(domain, requestId));
  });
}

function removeRequest(domain: string, requestId: string) {
  domains = {
    ...domains,
    [domain]: {
      ...domains[domain],
      requests: omit(domains[domain].requests, requestId)
    }
  };
}

export function pushResponse(domain: string, uuid: string, response: any) {
  if (!domains[domain] || !domains[domain].requests[uuid]) {
    return false;
  }

  domains[domain].requests[uuid].resolve(response);
  removeRequest(domain, uuid);

  return true;
}

type Listener = (requestId: string, req: Express.Request) => void;

export function addListener(domain: string, listener: Listener) {
  if (!domains[domain]) {
    domains = { ...domains, [domain]: { listeners: [], requests: {} } };
  }

  const domainModel = domains[domain];

  domains = {
    ...domains,
    [domain]: {
      ...domainModel,
      listeners: [...domainModel.listeners, listener]
    }
  };

  const unsubscribe = () => {
    const { listeners, requests } = domains[domain];
    const nextListeners = without(listeners, listener);

    if (!nextListeners.length) {
      Object.values(requests).forEach(r => r.reject("no more listeners"));
      domains = omit(domains, domain);
    } else {
      domains = {
        ...domains,
        [domain]: { ...domains[domain], listeners: nextListeners }
      };
    }
  };

  return unsubscribe;
}
