/**
 * The api to stream http requests and create responses for them.
 * It is assumed, that the api is hosted on a subdomain.
 */

import { Router, Request } from "express";

import * as requestBus from "./request-bus";

const api = Router();

api.get("/:hostname/requests", async (req, res) => {
  const hostname = req.params.hostname;
  console.log(`[api] listening to: ${hostname}`);

  req.setTimeout(0, () => {
    // TODO: handle the timeout?
  });

  const response = res.status(200).set({
    connection: "keep-alive",
    "cache-control": "no-cache",
    "content-type": "text/event-stream"
  });

  const unsubscribe = requestBus.addListener(hostname, (requestId, request) => {
    response.write(`data: ${JSON.stringify({ uid: requestId, request })}\n\n`);
  });
  res.on("close", unsubscribe);

  res.flushHeaders();
});

api.post("/:hostname/requests/:uid/responses", async (req, res) => {
  const hostname = req.params.hostname;
  console.log(`[api] response: ${hostname}/${req.params.uid}`);
  console.debug(req.body);

  try {
    const ok = requestBus.pushResponse(hostname, req.params.uid, req.body);

    if (ok) {
      res.end();
    } else {
      res.status(400).end();
    }
  } catch (err) {
    res.status(500).send(err.message || err);
  }
});

export default api;
