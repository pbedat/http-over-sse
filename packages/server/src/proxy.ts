/**
 * The proxy accepts all http requests and redirects it to its streaming listeners.
 */

import { Router } from "express";
import * as requestBus from "./request-bus";

function createProxy(debug: boolean) {
  const proxy = Router();

  // just for local dev
  proxy.all(`${debug ? "/:domain/" : ""}*`, async (req, res) => {
    try {
      console.log(`[proxy] request: ${req.method} ${req.hostname} ${req.url}`);

      const path = debug
        ? req.path.replace(new RegExp(`^\/${req.params.domain}`), "")
        : req.path;

      const response = await requestBus.pushRequest(req.hostname, path, req);
      console.log("[proxy] response: ", response);

      res.writeHead(response.status, response.headers);
      if (response.body) {
        res.write(response.body, response.encoding as string);
      }
      res.end();
    } catch (err) {
      res.status(400).send(err.message || err);
    }
  });

  return proxy;
}

export default createProxy;
