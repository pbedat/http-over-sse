import yargs from "yargs";
import { runServer } from "./server";

const { hostname, port, debug } = yargs
  .option("port", { type: "number", default: 80 })
  .option("debug", { type: "boolean", default: false })
  .option("hostname", { type: "string", default: "127.0.0.1" }).argv;

main();

async function main() {
  if (debug) {
    console.log("## DEBUG MODE ##");
  }
  await runServer({ port, hostname, debug });
  process.stdout.write("server started\n");
}
