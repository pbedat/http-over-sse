import express from "express";
import createBeacon from "pbedat-dashboard-middleware";
import cors from "cors";

import proxy from "./proxy";
import api from "./api";

interface ServerConfig {
  debug: boolean;
  port: number;
  hostname: string;
}

export function runServer(config: ServerConfig): Promise<Express.Application> {
  const { beacon } = createBeacon({
    serviceId: "http-over-sse",
    streamBinInstanceUrl: "https://streambin.pbedat.de"
  });

  const app = express();

  app.use(beacon);
  app.use(express.json({ limit: "5mb" }));
  app.use(express.static("./public"));
  app.use(cors());

  app.get("/", (req, res) => {
    res.send("super-eighty");
  });

  app.use("/api", api);
  app.use("/proxy", proxy(config.debug));

  return new Promise(resolve =>
    app.listen(config.port, config.hostname, () => {
      process.stdout.write(
        `[server] listening to ${config.hostname}:${config.port}\n`
      );
      resolve(app);
    })
  );
}
